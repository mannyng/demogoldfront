import 'babel-polyfill';
import 'whatwg-fetch';

import 'sanitize.css/sanitize.css';

import intl from 'intl';
import React from 'react';
import ReactDOM from 'react-dom';
//import '../../node_modules/bootstrap/dist/css/bootstrap.min.css';
import '../../node_modules/toastr/build/toastr.min.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import App from './App';

// global styles
import './style.scss';

// apply polyfill
if (!window.Intl) {
  window.Intl = intl;
}


ReactDOM.render(<App />, document.getElementById('app'));
