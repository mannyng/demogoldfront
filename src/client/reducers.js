import {combineReducers} from 'redux';
import {routerReducer} from 'react-router-redux';
import admin from './reducers/adminReducer';
import authen from './components/Authentication/authReducers';
import assets from './components/AssetsFolder/assetsReducers';
import news from './components/NewsFolder/newsReducer';
import ajaxCallsInProgress from './constants/ajaxStatusReducer';

export default combineReducers({
 admin,
 authen,
 assets,
 news,
 ajaxCallsInProgress,
});
