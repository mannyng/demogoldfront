import * as types from '../constants/actionTypes';
import supplyApi from '../api/mockSupply';

//LOAD SUPPLIES FUNCTION
export function loadSupplies() {
    return function(dispatch) {
        //dispatch(beginAjaxCall());
        return supplyApi.getAllSupplies().then(supplies => {
            dispatch(loadSuppliesSuccess(supplies));
        }).catch(error => {
            throw(error);
        });
    };
}
export function loadSuppliesSuccess(supplies) {
    //debugger;
    return { type: types.LOAD_SUPPLIES_SUCCESS, supplies};
}
