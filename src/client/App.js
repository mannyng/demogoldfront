import React from 'react';
import {hot} from 'react-hot-loader';
import {Provider} from 'react-redux';
import jwt_decode from 'jwt-decode';
import localStorage from 'localStorage';
import store from './store';
import Router from './router';


import { AUTH_USER} from './constants/actionTypes';
import {authenTokenSuccess,authenEmailSuccess,authenUserIdSuccess} from './components/Authentication/authActions';

//Authenticate
const token = localStorage.getItem('token');
if (token){
    store.dispatch({ type: AUTH_USER });
    const userId = jwt_decode(token).user_id;
    const emailID = jwt_decode(token).email_id;
    //debugger;
    //store.dispatch(authenTokenSuccess(token));
    store.dispatch(authenEmailSuccess(emailID));
    store.dispatch(authenUserIdSuccess(userId));
}
class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
            <Router />
      </Provider>
    );
  }
}

export default hot(module)(App);
