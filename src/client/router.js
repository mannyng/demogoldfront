import React from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';

import requireAuth from 'hoc/require_auth';
import NotAuthentication from 'hoc/no_require_auth';
import AppLayout from 'containers/AppLayout';
import LoginPage from 'containers/LoginPage';

export default function() {
  //<Route path="/login" name="login" component={NotAuthentication(LoginForm)} />
  //<Route path="/" name="home" component={NotAuthentication(AppLayout)} />
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/signin" name="login" component={NotAuthentication(LoginPage)} />
        <Route path="/" name="home" component={AppLayout} />
      </Switch>
    </BrowserRouter>
  );
}
