export default {
    ajaxCallsInProgress: 0,
    admin: {
    supplies: [],
    userId: 0
    },
    authen: {
      userId: 0,
		  emailID: '',
		  auth_token: '',
      authenticated: false,
      customerprofile: {},
      newcustomerprofile: {}
    },
    news: {
      marketnews: [],
      balticnews: [],
      allnews: []
    },
    assets: {
      alltransactions: [],
      account_balance: {},
      customerprofile: {},
      buyasset: '',
      sellasset: '',
      topupcash: '',
      allcustomers: [],
      allaccounts: []
    }
};
