import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import * as actions from '../components/Authentication/authActions';

export default function (ComposedComponent) {
  class NotAuthentication extends Component {
    componentWillMount() {
      if (this.props.authenticated) {
        this.redirectOut();
      }
    }

    componentWillUpdate(nextProps) {
      if (nextProps.authenticated) {
        this.context.router.history.push('/');
      }
    }
   redirectOut(){
     //debugger;
     this.context.router.history.push('/');
     //this.props.actions.authRedirect(this.context.router.history);
   }

    render() {
      return <ComposedComponent {...this.props} />;
    }
  }
  NotAuthentication.contextTypes = {
    router: PropTypes.object.isRequired
  };

  NotAuthentication.propTypes = {
      router: PropTypes.object,
      authenticated: PropTypes.bool.isRequired,
      userId: PropTypes.number.isRequired,
      history: PropTypes.object.isRequired
  };

  function mapStateToProps(state) {
    return {
      //auth_token: state.authen.auth_token,
      authenticated: state.authen.authenticated,
      userId: state.authen.userId
    };
  }
  return connect(mapStateToProps)(NotAuthentication);
}
