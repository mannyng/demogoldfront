import * as types from '../constants/actionTypes';
import initialState from '../initialState';

export default function (state = initialState.admin, action) {
    switch (action.type) {
    case types.LOAD_SUPPLIES_SUCCESS:
      //debugger;
      return {
        ...state,
        supplies: action.supplies
        };
   default:
   return state;
  }
}
