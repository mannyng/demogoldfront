import delay from './delay';

// This file mocks a web API by working with the hard-coded data below.
// It uses setTimeout to simulate the delay of an AJAX call.
// All calls return promises.
const supplies = [
  {
    supplierId: 23,
    sku: "ATB-90872-BU78-0087",
    cur_inventory_level: 0,
    supplier_sku: "FFN-BA-9315",
    product_name: "Swarovski",
    brand: "AF-32M",
    category: "Accessories,Women's Accessories, Jewellery, Earrings",
    color: "White",
    size: "One Size",
    retail_price: 990.00
  },
  {
    supplierId: 34,
    sku: "ATB-90872-BU78-0089",
    cur_inventory_level: 0,
    supplier_sku: "FFN-BA-9316",
    product_name: "Pearl Earrings - White",
    brand: "AFQ-32M",
    category: "Accessories,Women's Accessories, Jewellery, Earrings",
    color: "White",
    size: "One Size",
    retail_price: 1090.00
  },
  {
    supplierId: 34,
    sku: "ATB-32072-BU78-1092",
    cur_inventory_level: 0,
    supplier_sku: "FFN-BA-43311",
    product_name: "Pearl Earrings - White",
    brand: "AFM-212",
    category: "Accessories,Women's Accessories, Jewellery, Rings",
    color: "White",
    size: "One Size",
    retail_price: 1090.00
  },
  {
    supplierId: 23,
    sku: "ATB-072-BU78-0092",
    cur_inventory_level: 0,
    supplier_sku: "FNA-BA-43322",
    product_name: "Lady Classic Swarovski",
    brand: "AFM-982",
    category: "Accessories,Women's Accessories, Jewellery, Rings",
    color: "Silver",
    size: "5",
    retail_price: 1890.00
  },
  {
    supplierId: 23,
    sku: "ATB-072-BU78-0093",
    cur_inventory_level: 0,
    supplier_sku: "FNA-BA-43333",
    product_name: "Lady Classic Swarovski",
    brand: "AFM-982",
    category: "Accessories,Women's Accessories, Jewellery, Rings",
    color: "Silver",
    size: "6",
    retail_price: 1890.00
  },
  {
    supplierId: 23,
    sku: "ATB-072-BU78-0094",
    cur_inventory_level: 0,
    supplier_sku: "FNA-BA-43344",
    product_name: "Lady Classic Swarovski",
    brand: "AFM-982",
    category: "Accessories,Women's Accessories, Jewellery, Rings",
    color: "Silver",
    size: "7",
    retail_price: 1890.00
  },
  {
    supplierId: 23,
    sku: "ATB-072-BU78-0095",
    cur_inventory_level: 0,
    supplier_sku: "FNA-BA-43355",
    product_name: "Lady Classic Swarovski",
    brand: "AFM-982",
    category: "Accessories,Women's Accessories, Jewellery, Rings",
    color: "Silver",
    size: "8",
    retail_price: 1890.00
  },
  {
    supplierId: 23,
    sku: "ATB-072-BU78-0096",
    cur_inventory_level: 0,
    supplier_sku: "FNA-BA-43366",
    product_name: "Lady Classic Swarovski",
    brand: "AFM-982",
    category: "Accessories,Women's Accessories, Jewellery, Rings",
    color: "Silver",
    size: "9",
    retail_price: 1890.00
  },
  {
    supplierId: 23,
    sku: "ATB-072-BU78-0995",
    cur_inventory_level: 0,
    supplier_sku: "FNA-BA-54355",
    product_name: "Trinity Rings Swarovski",
    brand: "AFM-984",
    category: "Accessories,Women's Accessories, Jewellery, Rings",
    color: "Silver",
    size: "5",
    retail_price: 1490.00
  }
];

function replaceAll(str, find, replace) {
  return str.replace(new RegExp(find, 'g'), replace);
}

//This would be performed on the server in a real app. Just stubbing in.
const generateSku = (supply) => {
  counter += 11;
  return replaceAll('ATB-072-BU78', ' ', '-', counter);
};

class SupplyApi {
  static getAllSupplies() {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(Object.assign([], supplies));
      }, delay);
    });
  }

  static saveSupply(supply) {
    supply = Object.assign({}, supply); // to avoid manipulating object passed in.
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        // Simulate server-side validation
        const minSupplyProductNameLength = 1;
        if (supply.product_name.length < minSupplyProductNameLength) {
          reject(`Title must be at least ${minSupplyProductNameLength} characters.`);
        }

        if (supply.sku) {
          const existingSupplyIndex = supplies.findIndex(a => a.sku == supply.sku);
          courses.splice(existingSupplyIndex, 1, supply);
        } else {
          //Just simulating creation here.
          //The server would generate ids and watchHref's for new courses in a real app.
          //Cloning so copy returned is passed by value rather than by reference.
          supply.sku = generateSku(supply);
          supplies.push(supply);
        }

        resolve(supply);
      }, delay);
    });
  }

  static deleteSupply(supplySku) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        const indexOfSupplyToDelete = supplies.findIndex(supply => {
          supply.sku == supplySku;
        });
        supplies.splice(indexOfSupplyToDelete, 1);
        resolve();
      }, delay);
    });
  }
}

export default SupplyApi;

