import React from 'react';

import {PageHeader} from 'react-bootstrap';

export default function NotFound() {
  return (
    <section style={{padding: 20}}>
      <PageHeader>Page not found.</PageHeader>
    </section>
  );
}
