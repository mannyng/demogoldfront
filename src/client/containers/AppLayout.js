import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {Redirect, Route, Switch} from 'react-router-dom';
import NotFound from './NotFound';
import Supplies from './Supplies';
import { Grid,ProgressBar } from 'react-bootstrap';
import Headerfirst from '../components/Headers/headerFirst';
import FirstFooter from '../components/Footers/firstFooter';
import NewsPage from '../components/NewsFolder/NewsPage';
import LoginForm from './LoginForm';
import LoginPage from '../components/Authentication/LoginPage';

import requireAuth from '../hoc/require_auth';
import NotAuthentication from '../hoc/no_require_auth';

class AppLayout extends React.Component {

  render() {
    const {loading} = this.props;
    //debugger;
   return(
     <Grid fluid>
          <Headerfirst />
           {loading > 0 && <ProgressBar />}
         <section style={{paddingTop: 50}}>
           <Switch>
             <Route exact path="/" component={Supplies} />
             <Route exact path="/users" name="home-users" component={NotFound} />
             <Route exact path="/profile" name="home-profile" component={requireAuth(Supplies)} />
             <Route exact path="/login" name="login_page"  component={NotAuthentication(LoginForm)}/>
             <Route exact path="/sign_in" name="signin_page"  component={NotAuthentication(LoginPage)}/>
             <Redirect to="/" />
           </Switch>
         </section>
         <section style={{marginTop: 50}}>
         <FirstFooter/>
         </section>
       </Grid>
   )
 }
}
AppLayout.propTypes = {
  loading: PropTypes.number.isRequired,
}
function mapStateToProps(state) {
  return {
    loading: state.ajaxCallsInProgress
  };
}
export default connect(mapStateToProps)(AppLayout);
