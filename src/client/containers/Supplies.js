import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import {connect} from 'react-redux';
import * as actions from '../actions/adminActions';
import { Grid,Jumbotron,Button,Table } from 'react-bootstrap';
import AssetsPage from '../components/AssetsFolder/AssetsPage';
import TransactsPage from '../components/AssetsFolder/transactPage';

class Supplies extends React.Component{
  constructor(props) {
    super(props);

    this.state = {
      supplierID: '',
    };
  }
  componentWillMount() {
   this._retrieveSupplies();
  }

  _retrieveSupplies() {
   this.props.actions.loadSupplies();
  }
  handleChange = name => event => {
    this.setState({
      [name]: event.target.value,
    });
  };

  render(){
    const {supplies} = this.props;
   //debugger;
  return(
    <Grid style={{padding: 20}}>
    <section style={{marginTop: 50}}>
    <AssetsPage />
    <hr/>
    <TransactsPage/>
    <hr/>
    </section>
     </Grid>
    );
  }
}

Supplies.propTypes = {
  supplies: PropTypes.array.isRequired,
};

function mapStateToProps(state, ownProps) {
  return {
    supplies: state.admin.supplies
  };
}
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  };
}
export default connect(mapStateToProps,mapDispatchToProps)(Supplies);
