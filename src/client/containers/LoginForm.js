import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import {connect} from 'react-redux';
import {Redirect, Route, Switch} from 'react-router-dom';
import AuthForm from '../components/Authentication/authForm';
import RegisterForm from '../components/Authentication/registerUser';
import * as actions from '../components/Authentication/authActions';
import Headerfirst from '../components/Headers/headerFirst';
import axios from 'axios';

class LoginForm extends React.Component {
  constructor(...args) {
    super(...args);
    this.state = {
      email: '',
      password: '',
      password_confirmation: '',
      remember_me: false,
      submitted: false
    };

    this.handleChange = this.handleChange.bind(this);
    this._loginUser = this._loginUser.bind(this);
    this._registerUser = this._registerUser.bind(this);
    //this.getValidationState = this.getValidationState.bind(this);
  }
  //componentWillMount() {
		//this._retrieveUsers();
//	}

	//componentWillReceiveProps(nextProps) {
	//	if (nextProps.users && nextProps.users) {
	//		this.setState({ isLoading: false });
	//	}
	//}

   _loginUser(){
     debugger;
    const {email,password} = this.state;
    const values = {email,password};
    console.log(values);
      //this.props.actions.signinUser(values,this.context.router.history);
      return {
      callAPI: () => fetch('http://localhost:3002/users/login',
  {
    method: 'post',
    headers:{
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({values})
  }),
  payload: {values}
}
debugger;
};
_retrieveUsers(isRefreshed) {
  this.props.actions.fetchUserProfiles();
  if (isRefreshed && this.setState({ isRefreshing: false }));
};
handleChange = key => (event) => {
    //debugger;
  this.setState({
    [key]: event.target.value
  });
};

   getValidationState = key => (event) => {
     //debugger;
    const length = this.state.password.length;
    if (length > 10) return 'success';
    else if (length > 5) return 'warning';
    else if (length > 0) return 'error';
    return null;
  };
   _registerUser(){
     debugger;
    const {email,password,password_confirmation,remember_me} = this.state;
    const values = {email,password,password_confirmation,remember_me};
    console.log(values);
      this.props.actions.registerUser(values,this.context.router.history);
   };

  render() {
   const {email,password,password_confirmation,remember_me, submitted} = this.state;
   //<RegisterForm
  // email={email}
  // password={password}
  // password_confirmation={password_confirmation}
  // remember_me = {remember_me}
  // _registerUser={this._registerUser}
  // handleChange={this.handleChange}
//   />
   return(
     <div>

    <h1>Login</h1>

    <AuthForm
      handleChange={this.handleChange}
      _loginUser={this._loginUser}
      email={email}
     password={password}
     submitted={submitted}
    />

    </div>
   )
 }
}
LoginForm.contextTypes = {
  router: PropTypes.object.isRequired
};
LoginForm.propTypes = {
  auth_token: PropTypes.string.isRequired,
  actions: PropTypes.object.isRequired,
  userId: PropTypes.number.isRequired,
  emailID: PropTypes.string.isRequired,
};

function mapStateToProps(state, ownProps) {
 //debugger;
  return {
  auth_token: state.authen.auth_token,
	userId: state.authen.userId,
	emailID: state.authen.emailID
  };
}
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginForm);
