import React from 'react';
import {OverlayTrigger,ButtonToolbar,Button,Popover,Well} from 'react-bootstrap';

class ClientPopover extends React.Component{

  render(){
    const popoverClickRootClose = (
     <Popover id="popover-trigger-click-root-close" title="Current Standing">
        <strong>Holy guacamole!</strong> Check this info.
        <Well><p>All accounts payable with number {''}</p>
        <p>All the account acrueble are based on {' '}</p>
        </Well>
     </Popover>);
    return (
     <ButtonToolbar c>
       <OverlayTrigger
        trigger="click" rootClose
         placement="bottom"
         overlay={popoverClickRootClose} >
         <Button bsStyle="success">Clients</Button>
        </OverlayTrigger>
      </ButtonToolbar>
    )
  };
}

export default ClientPopover;
