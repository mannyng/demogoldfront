import React, { Component } from 'react';
import DropdownButton from './DropdownButton';
import MenuItem from './MenuItem';
import ButtonToolbar from './ButtonToolbar';

class Headerfirst extends Component {
    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.state = {
            isOpen: false
        };
    }
    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }
    render() {
      const BUTTONS = ['Default', 'Primary', 'Success', 'Info', 'Warning', 'Danger'];
      function renderDropdownButton(title, i) {
      return (
        <DropdownButton
          bsStyle={title.toLowerCase()}
          title={title}
          key={i}
          id={`dropdown-basic-${i}`}
        >
          <MenuItem eventKey="1">Action</MenuItem>
          <MenuItem eventKey="2">Another action</MenuItem>
          <MenuItem eventKey="3" active>
            Active Item
          </MenuItem>
          <MenuItem divider />
          <MenuItem eventKey="4">Separated link</MenuItem>
        </DropdownButton>
      );
      }

      const buttonsInstance = (
      <ButtonToolbar>{BUTTONS.map(renderDropdownButton)}</ButtonToolbar>
      );

        return (
            <div>


        {buttonsInstance}
            </div>
        );
    }
}

export default Headerfirst;
