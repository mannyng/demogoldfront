import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import {connect} from 'react-redux';
import { Grid,Navbar,Nav,NavItem,MenuItem,NavDropdown } from 'react-bootstrap';
import * as actions from '../Authentication/authActions';
import ClientPopover from './clientPopover';

const divStyle = {
  marginRight: 20,
};
class Headerfirst extends Component {
    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.state = {
            isOpen: false
        };
        this._logoutUser = this._logoutUser.bind(this);
    }
    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }
    _logoutUser(){
       this.props.actions.resetAuth(this.context.router.history);
   }
    render() {
     const {emailID} = this.props;

     //debugger;
        return (
          <Grid>
            <Navbar inverse collapseOnSelect>
             <Navbar.Header>
                <Navbar.Brand>
                 <a href="/">Gold To Cash Place</a>
                </Navbar.Brand>
                <Navbar.Toggle />
              </Navbar.Header>
             <Navbar.Collapse>
             <Nav>
               <NavItem eventKey={1} href="#">
                Link
               </NavItem>
               <NavItem eventKey={2} href="#">
                 Link
               </NavItem>
               <NavDropdown eventKey={3} title="Dropdown" id="basic-nav-dropdown">
                 <MenuItem eventKey={3.1}>Action</MenuItem>
                 <MenuItem eventKey={3.2}>Another action</MenuItem>
                 <MenuItem eventKey={3.3}>Something else here</MenuItem>
                 <MenuItem divider />
                 <MenuItem eventKey={3.3}>Separated link</MenuItem>
               </NavDropdown>
             </Nav>
             <Nav pullRight style={divStyle}>
               <NavItem eventKey={1} href="/profile">
                 Link Right
               </NavItem>
               <NavItem eventKey={2} href="/users">
                 Link Right
               </NavItem>
               <NavDropdown eventKey={3} title="User Account" id="basic-nav-dropdown">
                 <MenuItem eventKey={3.1} href="/login">Profile</MenuItem>
                 <MenuItem eventKey={3.2} href="/sign_in">Setting</MenuItem>
                 <MenuItem eventKey={3.3}>Details</MenuItem>
                 <MenuItem divider />
                 <MenuItem eventKey={3.3}>Information</MenuItem>
                 {this.props.authenticated &&
                   <button className="btn btn-primary"
                   onClick={this._logoutUser}>
                     Log Out {' '}{this.props.emailID}
                   </button>
                 }
               </NavDropdown>
               <NavItem eventKey={4}>
                <ClientPopover/>
              </NavItem>
             </Nav>
            </Navbar.Collapse>
          </Navbar>
         </Grid>
        );
    }
}
Headerfirst.contextTypes = {
  router: PropTypes.object.isRequired
};
Headerfirst.propTypes = {
  auth_token: PropTypes.string.isRequired,
  actions: PropTypes.object.isRequired,
  userId: PropTypes.number.isRequired,
  emailID: PropTypes.string,
  authenticated: PropTypes.bool
  //lotto: PropTypes.array.isRequired
};

function mapStateToProps(state, ownProps) {
 //debugger;
  return {
  auth_token: state.authen.auth_token,
	userId: state.authen.userId,
	emailID: state.authen.emailID,
  authenticated: state.authen.authenticated
	//lotto: state.customer.lotto
  };
}
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Headerfirst);
//export default Headerfirst;
