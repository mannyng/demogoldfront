import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import {connect} from 'react-redux';
import * as actions from './assetsActions';
import { Grid,Button,Well,Col,Row,Table} from 'react-bootstrap';
import toastr from 'toastr';

class AssetsPage extends React.Component{
  constructor(props) {
    super(props);

    this.state = {
      cash_balance: 0.00,
      gold_balance: 0.00,
      txref: '',
      transaction_type: '',
      asset: '',
      amount: 0.00,
      submitted: false,
      firstname: '',
      lastname: '',
      phone: '',
      customer_id: ''
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleCustomerAccountBalance = this.handleCustomerAccountBalance.bind(this);
    this._handleCreateCustomer = this._handleCreateCustomer.bind(this);
    this._fetchAllCustomers = this._fetchAllCustomers.bind(this);
    this.handleBalanceTopup = this.handleBalanceTopup.bind(this);
  }
  componentWillMount() {
   this._fetchAllAccounts();
   this._fetchAllCustomers();
  }

  _fetchAllCustomers() {
   this.props.actions.fetchAllCustomers();
  }
  _fetchAllAccounts() {
   this.props.actions.fetchAllAccounts();
  }
  _checkCustomerBalance() {
    const {CustomerID} = this.props.CustomerID;
   this.props.actions.fetchCustomerBalance(CustomerID);
  }
  _checkAllTransactions() {
   this.props.actions.fetchAllTransactions();
  }
  handleChange(e) {
    debugger;
      const { name, value } = e.target;
      this.setState({ [name]: value });
  }

  handleSubmit(e) {
      e.preventDefault();
      const {customerID} = this.props.customerprofile.id
      this.setState({ submitted: true });
        debugger;
          this.props.actions.createCustomerBalance(customerID,this.context.router.history);
  }
  handleBalanceTopup(e) {
    e.preventDefault();
    this.setState({ submitted: true });
    debugger;
    const balance_id = this.props.account_balance.id
    const customer_id = this.props.CustomerID;
    const { transaction_type,asset,amount } = this.state;
    const values = {balance_id,transaction_type,asset,amount,customer_id };
    if (transaction_type == 'topup'){
    debugger;
    this.props.actions.topUpCustomerAccount(values,this.context.router.history);
   }else if (transaction_type == 'buy') {
     debugger;
    this.props.actions.customerBuyGold(values,this.context.router.history);
   }else if (transaction_type == 'sell') {
     debugger;
    this.props.actions.customerSellGold(values,this.context.router.history);
   }else {
    toastr.error('Bad info, try again')
   }
   this._checkCustomerProfile();
  }
  handleCustomerAccountBalance(e) {
    e.preventDefault();
    //this._checkCustomerProfile();
    this.setState({ submitted: true });
    debugger;
    //const customer_id = this.props.customerprofile.id
    const { customer_id } = this.state;
    const values = {customer_id};
    debugger;
    this.props.actions.createCustomerAccountBalance(values,this.context.router.history);
  }

  _handleCreateCustomer(e) {
    e.preventDefault();
    this.setState({ submitted: true });
    const user_id = this.props.userId;
    const { firstname,lastname,phone } = this.state;
    const values = {user_id,firstname,lastname,phone};
    debugger;
    this.props.actions.createCustomerProfile(values,this.context.router.history);
  }

  render(){
    const {customerprofile,alltransactions,account_balance,allcustomers,
    allaccounts} = this.props;
    const {transaction_type,asset,amount,firstname,lastname,phone,submitted,
    customer_id} = this.state;
   //debugger;
  return(
    <Grid style={{padding: 20}}>
    <Well bsSize="large">
    <Row className="show-grid">

    <Col md={4} xs={12}>
    <form name="form" onSubmit={this._handleCreateCustomer}>
    <Col className={'form-group' + (submitted && !firstname ? ' has-error' : '')}>
        <label htmlFor="firstname">First Name</label>
        <input type="text" className="form-control" name="firstname" value={firstname} onChange={this.handleChange} />
        {submitted && !firstname &&
            <Col className="help-block">First Name is required</Col>
        }
    </Col>
    <Col className={'form-group' + (submitted && !lastname ? ' has-error' : '')}>
        <label htmlFor="lastname">Last Name</label>
        <input type="text" className="form-control" name="lastname" value={lastname} onChange={this.handleChange} />
        {submitted && !lastname &&
            <Col className="help-block">Last Name is required</Col>
        }
    </Col>
    <Col className={'form-group' + (submitted && !phone ? ' has-error' : '')}>
        <label htmlFor="phone">Phone</label>
        <input type="text" className="form-control" name="phone" value={phone} onChange={this.handleChange} />
        {submitted && !phone &&
            <Col className="help-block">Phone is required</Col>
        }
    </Col>
    <Col className="form-group">
        <button className="btn btn-primary">Create Customer</button>
     </Col>
     </form>
    </Col>
    <Col md={2} xs={12}>

    </Col>
    <Col md={6} xs={12}>
    <div style={{height:200, overflow: "scroll"}}>
      <p>Customers Table</p>
    <Table striped bordered condensed hover responsive>
   <thead>
     <tr>
       <th>#</th>
       <th>FirstName</th>
       <th>LastName</th>
       <th>Phone</th>
       <th>Actions</th>
       </tr>
      </thead>
    <tbody>

    {allcustomers && allcustomers.map((customer, index) =>
      <tr key={index}>
      <td>{index}</td>
      <td> {customer.firstname}</td>
      <td> {customer.lastname}</td>
      <td>{customer.phone}</td>
      <td>
      <form name="form" onSubmit={this.handleCustomerAccountBalance}>

      <button className="btn btn-primary" name="customer_id"
       value={customer.id} onClick={this.handleChange}>Create Balance</button>
      </form>
      </td>
      </tr>
    )}
    </tbody>
   </Table>
   </div>
   <div style={{height:200, overflow: "scroll"}}>
     <p>Accounts Table</p>
   <Table striped bordered condensed hover responsive>
  <thead>
    <tr>
      <th>#</th>
      <th>Customer ID</th>
      <th>Gold Balance</th>
      <th>Cash Balance</th>
      <th>Actions</th>
      </tr>
     </thead>
   <tbody>

   {allaccounts && allaccounts.map((account, index) =>
     <tr key={index}>
     <td>{index}</td>
     <td> {account.customer_id}</td>
     <td> {account.gold_balance}</td>
     <td>{account.cash_balance}</td>
     <td>
     <form name="form" onSubmit={this.handleCustomerAccountBalance}>

     <button className="btn btn-primary" name="balance_id"
      value={account.id} onClick={this.handleChange}>Delete Balance</button>
     </form>
     </td>
     </tr>
   )}
   </tbody>
  </Table>
  </div>
   </Col>
    </Row>
    </Well>
     </Grid>
    );
  }
}
AssetsPage.contextTypes = {
  router: PropTypes.object.isRequired
};
AssetsPage.propTypes = {
  actions: PropTypes.object.isRequired,
  allcustomers: PropTypes.array.isRequired,
  alltransactions: PropTypes.array.isRequired,
  customerprofile: PropTypes.object.isRequired,
  userId: PropTypes.number.isRequired,
  account_balance: PropTypes.object.isRequired,
  allaccounts: PropTypes.array.isRequired,
};

function mapStateToProps(state, ownProps) {
  return {
    account_balance: state.assets.account_balance,
    alltransactions: state.assets.alltransactions,
    userId: state.authen.userId,
    customerprofile: state.authen.customerprofile,
    allcustomers: state.assets.allcustomers,
    allaccounts: state.assets.allaccounts,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  };
}
export default connect(mapStateToProps,mapDispatchToProps)(AssetsPage);
