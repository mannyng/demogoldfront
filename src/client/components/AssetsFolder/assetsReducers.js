import * as types from '../../constants/actionTypes';
import initialState from '../../initialState';

export default function (state = initialState.assets, action) {
    switch (action.type) {
        case types.FETCH_ALL_TRANSACTION_SUCCESS:
                 return {
                 ...state,
                 alltransactions: action.alltransactions
         };
         case types.CREATE_CUSTOMER_ACCOUNT_SUCCESS:
                  return {
                  ...state,
                  customerprofile: action.customerprofile
          };
           case types.FETCH_CUSTOMER_BALANCE_SUCCESS:
                    return {
                    ...state,
                    account_balance: action.account_balance
            };
            case types.TOPUP_ACCOUNT_BALANCE_SUCCESS:
                     return {
                     ...state,
                     topupcash: action.topupcash
             };
            case types.BUY_TRADE_ASSET_SUCCESS:
                     return {
                     ...state,
                     buyasset: action.buyasset
             };
             case types.SELL_TRADE_ASSET_SUCCESS:
                      return {
                      ...state,
                      sellasset: action.sellasset
              };
              case types.FETCH_ALL_CUSTOMER_SUCCESS:
                       return {
                       ...state,
                       allcustomers: action.allcustomers
               };
               case types.FETCH_ALL_BALANCE_SUCCESS:
                        return {
                        ...state,
                        allaccounts: action.allaccounts
                };

      default:
        return state;
    }
  }
