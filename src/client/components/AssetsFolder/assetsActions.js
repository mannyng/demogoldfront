import * as types from '../../constants/actionTypes';
import axios from 'axios';
import toastr from 'toastr';
import localStorage from 'localStorage';

//FETCH ALL TRANSACTIONS
export function fetchAllTransactions(){
  const mytoken = localStorage.getItem('token');
  const GAPI_HEADERS = {
       'Content-Type': 'application/json',
       'Authorization': `Bearer ${mytoken}`
   };
  //debugger;
  return function(dispatch){
    axios.get(`${types.ROOT_URL}/transactions`,
    {headers: GAPI_HEADERS })
      .then(response => {
          //debugger;
          dispatch(fetchAllTransactionsSuccess(response.data));
        })
        .catch(error => {
          //debugger;
          throw(error);
        })
  };
}
export function fetchAllTransactionsSuccess(alltransactions){
  //debugger;
  return {
    type: types.FETCH_ALL_TRANSACTION_SUCCESS,
    alltransactions: alltransactions
  }
}
//FETCH ALL CUSTOMERS
export function fetchAllCustomers(){
  const mytoken = localStorage.getItem('token');
  const GAPI_HEADERS = {
       'Content-Type': 'application/json',
       'Authorization': `Bearer ${mytoken}`
   };
  //debugger;
  return function(dispatch){
    axios.get(`${types.ROOT_URL}/customers`,
    {headers: GAPI_HEADERS })
      .then(response => {
        //  debugger;
          dispatch(fetchAllCustomersSuccess(response.data));
        })
        .catch(error => {
          debugger;
          throw(error);
        })
  };
}
export function fetchAllCustomersSuccess(allcustomers){
  //debugger;
  return {
    type: types.FETCH_ALL_CUSTOMER_SUCCESS,
    allcustomers: allcustomers
  }
}
//FETCH ALL BALANCES(ACCOUNTS)
export function fetchAllAccounts(){
  const mytoken = localStorage.getItem('token');
  const GAPI_HEADERS = {
       'Content-Type': 'application/json',
       'Authorization': `Bearer ${mytoken}`
   };
  debugger;
  return function(dispatch){
    axios.get(`${types.ROOT_URL}/balances`,
    {headers: GAPI_HEADERS })
      .then(response => {
          debugger;
          dispatch(fetchAllAccountsSuccess(response.data));
        })
        .catch(error => {
          debugger;
          throw(error);
        })
  };
}
export function fetchAllAccountsSuccess(allaccounts){
  debugger;
  return {
    type: types.FETCH_ALL_BALANCE_SUCCESS,
    allaccounts: allaccounts
  }
}
//CREATE ACCOUNT BALANCE FOR CUSTOMER
export function createCustomerBalance(customer_id,history){
  return function(dispatch){
    axios.post(`${types.ROOT_URL}/transactions`,{customer_id})
      .then(response => {
          debugger;
          dispatch(fetchAllTransactions());
        })
        .catch(error => {
          debugger;
          throw(error);
        })
  };
}

//FETCH CUSTOMER PROFILE
export function fetchCustomerProfile({user_id},history){
  const mytoken = localStorage.getItem('token');
  const GAPI_HEADERS = {
       'Content-Type': 'application/json',
       'Authorization': `Bearer ${mytoken}`
   };
  debugger;
  return function(dispatch){
    axios.get(`${types.ROOT_URL}/customers/${user_id}`,
    {headers: GAPI_HEADERS })
      .then(response => {
          debugger;
          dispatch(fetchCustomerProfileSuccess(response.data));
          dispatch(fetchCustomerBalance(response.data.id));
        })
        .catch(error => {
          debugger;
          throw(error);
        })
  };
}
export function fetchCustomerProfileSuccess(customerprofile){
  debugger;
  return {
    type: types.FETCH_CUSTOMER_PROFILE,
    customerprofile: customerprofile
  }
}
//FETCH CUSTOMER ACCOUNT BALANCE
export function fetchCustomerBalance(customer_id){
  const mytoken = localStorage.getItem('token');
  const GAPI_HEADERS = {
       'Content-Type': 'application/json',
       'Authorization': `Bearer ${mytoken}`
   };
  debugger;
  return function(dispatch){
    axios.get(`${types.ROOT_URL}/balances/${customer_id}`,
    {headers: GAPI_HEADERS })
      .then(response => {
          debugger;
          dispatch(fetchCustomerBalanceSuccess(response.data));
        })
        .catch(error => {
          debugger;
          throw(error);
        })
  };
}
export function fetchCustomerBalanceSuccess(account_balance){
  debugger;
  return {
    type: types.FETCH_CUSTOMER_BALANCE_SUCCESS,
    account_balance: account_balance
  }
}
//CREATE CUSTOMER
export function createCustomerProfile({user_id,firstname,lastname,phone},history){
  const mytoken = localStorage.getItem('token');
  const GAPI_HEADERS = {
       'Content-Type': 'application/json',
       'Authorization': `Bearer ${mytoken}`
   };
  debugger;
  return function(dispatch){
    axios.post(`${types.ROOT_URL}/customers`,{user_id,firstname,lastname,phone},
    {headers: GAPI_HEADERS })
      .then(response => {
          debugger;
          dispatch(createCustomerProfileSuccess(response.data));
        })
        .catch(error => {
          debugger;
          throw(error);
        })
  };
}
export function createCustomerProfileSuccess(newcustomerprofile){
  debugger;
  return {
    type: types.CREATE_CUSTOMER_SUCCESS,
    newcustomerprofile: newcustomerprofile
  }
}

//CREATE CUSTOMER ACCOUNT
export function createCustomerAccountBalance({customer_id},history){
  const mytoken = localStorage.getItem('token');
  const GAPI_HEADERS = {
       'Content-Type': 'application/json',
       'Authorization': `Bearer ${mytoken}`
   };
  debugger;
  return function(dispatch){
    axios.post(`${types.ROOT_URL}/balances`,{customer_id},
    {headers: GAPI_HEADERS })
      .then(response => {
          debugger;
          dispatch(createCustomerProfileSuccess(response.data));
        })
        .catch(error => {
          debugger;
          throw(error);
        })
  };
}

//TOP UP CUSTOMER ACCOUNT
export function topUpCustomerAccount({balance_id,transaction_type,asset,amount,customer_id},history){
  const mytoken = localStorage.getItem('token');
  const GAPI_HEADERS = {
       'Content-Type': 'application/json',
       'Authorization': `Bearer ${mytoken}`
   };
  debugger;
  return function(dispatch){
    axios.post(`${types.ROOT_URL}/transactions/top_up`,{balance_id,transaction_type,asset,amount},
    {headers: GAPI_HEADERS })
      .then(response => {
          debugger;
          dispatch(topUpCustomerAccountSuccess(response.data));
          dispatch(fetchCustomerBalance(customer_id));
          dispatch(fetchAllTransactions());
        })
        .catch(error => {
          debugger;
          throw(error);
        })
  };
}
export function topUpCustomerAccountSuccess(topupcash){
  debugger;
  return {
    type: types.TOPUP_ACCOUNT_BALANCE_SUCCESS,
    topupcash: topupcash
  }
}

//CUSTOMER BUY GOLD
export function customerBuyGold({balance_id,transaction_type,asset,amount,customer_id},history){
  const mytoken = localStorage.getItem('token');
  const GAPI_HEADERS = {
       'Content-Type': 'application/json',
       'Authorization': `Bearer ${mytoken}`
   };
  debugger;
  return function(dispatch){
    axios.post(`${types.ROOT_URL}/transactions/buy`,{balance_id,transaction_type,asset,amount},
    {headers: GAPI_HEADERS })
      .then(response => {
          debugger;
          dispatch(customerBuyGoldSuccess(response.data));
          dispatch(fetchCustomerBalance(customer_id));
          dispatch(fetchAllTransactions());
        })
        .catch(error => {
          debugger;
          throw(error);
        })
  };
}
export function customerBuyGoldSuccess(buyasset){
  debugger;
  return {
    type: types.BUY_TRADE_ASSET_SUCCESS,
    buyasset: buyasset
  }
}
//CUSTOMER SELL GOLD
export function customerSellGold({balance_id,transaction_type,asset,amount,customer_id},history){
  const mytoken = localStorage.getItem('token');
  const GAPI_HEADERS = {
       'Content-Type': 'application/json',
       'Authorization': `Bearer ${mytoken}`
   };
  debugger;
  return function(dispatch){
    axios.post(`${types.ROOT_URL}/transactions/sell`,{balance_id,transaction_type,asset,amount},
    {headers: GAPI_HEADERS })
      .then(response => {
          debugger;
          dispatch(customerSellGoldSuccess(response.data));
          dispatch(fetchCustomerBalance(customer_id));
          dispatch(fetchAllTransactions());
        })
        .catch(error => {
          debugger;
          throw(error);
        })
  };
}
export function customerSellGoldSuccess(sellasset){
  debugger;
  return {
    type: types.SELL_TRADE_ASSET_SUCCESS,
    sellasset: sellasset
  }
}
