import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import {connect} from 'react-redux';
import * as actions from './assetsActions';
import { Grid,Button,Well,Col,Row,Pager,Table,FormControl} from 'react-bootstrap';
import toastr from 'toastr';
//import SelectInput from '../common/SelectInput';

class TransactsPage extends React.Component{
  constructor(props) {
    super(props);

    this.state = {
      txref: '',
      transaction_type: '',
      asset: '',
      amount: 0.00,
      submitted: false
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleBalanceTopup= this.handleBalanceTopup.bind(this);
    this._checkCustomerProfile = this._checkCustomerProfile.bind(this);
    this._checkAllTransactions = this._checkAllTransactions.bind(this);
  }
  componentWillMount() {
   this._checkAllTransactions();
   this._checkCustomerProfile();
  }
  _checkAllTransactions(){
    this.props.actions.fetchAllTransactions();
  }
  _checkCustomerProfile() {
  //  debugger;
    const user_id = this.props.userId;
    const values = {user_id};
  //  debugger;
   this.props.actions.fetchCustomerProfile(values,this.context.router.history);
  }
  _checkAllTransactions() {
   this.props.actions.fetchAllTransactions();
  }
  handleChange(e) {
      const { name, value } = e.target;
      this.setState({ [name]: value });
  }

  handleBalanceTopup(e) {
    e.preventDefault();
    this.setState({ submitted: true });
    debugger;
    const balance_id = this.props.account_balance.id;
    const customer_id = this.props.account_balance.customer_id
    const { transaction_type,asset,amount } = this.state;
    const values = {balance_id,transaction_type,asset,amount,customer_id };
    if (transaction_type == 'topup'){
    debugger;
    this.props.actions.topUpCustomerAccount(values,this.context.router.history);
   }else if (transaction_type == 'buy') {
     debugger;
    this.props.actions.customerBuyGold(values,this.context.router.history);
   }else if (transaction_type == 'sell') {
     debugger;
    this.props.actions.customerSellGold(values,this.context.router.history);
   }else {
    toastr.error('Bad info, try again')
   }
  }

  render(){
    const {customerprofile,alltransactions,account_balance} = this.props;
    const {transaction_type,asset,amount,submitted} = this.state;
   //debugger;
  return(
    <Grid style={{padding: 20}}>
    <Well bsSize="large">
    <Row className="show-grid">
    <Col md={4} xs={12}>
    <form name="form" onSubmit={this.handleBalanceTopup}>
    <Col className={'form-group' + (submitted && !transaction_type ? ' has-error' : '')
       }>
     <FormControl componentClass="select" placeholder="select"
     onChange={this.handleChange} name="transaction_type">
       <option value="select">select</option>
       <option value="topup">topup</option>
       <option value="buy">buy</option>
       <option value="sell">sell</option>
     </FormControl>
        {submitted && !transaction_type &&
            <Col className="help-block">Trans Type is required</Col>
        }
    </Col>
    <Col className={'form-group' + (submitted && !asset ? ' has-error' : '')}>
        <label htmlFor="asset">Asset</label>
        <input type="text" className="form-control" name="asset" value={asset} onChange={this.handleChange} />
        {submitted && !asset &&
            <Col className="help-block">Asset Type is required</Col>
        }
    </Col>
    <Col className={'form-group' + (submitted && !amount ? ' has-error' : '')}>
        <label htmlFor="amount">Amount</label>
        <input type="text" className="form-control" name="amount" value={amount} onChange={this.handleChange} />
        {submitted && !amount &&
            <Col className="help-block">Amount is required</Col>
        }
    </Col>
        <Col className="form-group">
            <button className="btn btn-primary">Transact</button>
         </Col>
      </form>
    </Col>
    <Col md={2} xs={12}>

   </Col>
   <Col md={6} xs={12}>
   <div style={{height:250, overflow: "scroll"}}>
   <Table striped bordered condensed hover responsive>

  <thead>
    <tr>
      <th>#</th>
      <th>Asset</th>
      <th>Amount</th>
      <th>Type</th>
      <th>Ref</th>
      </tr>
     </thead>
   <tbody>

   {alltransactions.map((transaction, index) =>
     <tr key={index}>
     <td>{index}</td>
     <td> {transaction.asset}</td>
     <td> {transaction.amount}</td>
     <td>{transaction.transaction_type}</td>
     <td> {transaction.txref}</td>
     </tr>
   )}
   </tbody>
  </Table>
  </div>
  {account_balance &&
   <Col md={6} xs={12}>
    <p> Gold Balance:{account_balance.gold_balance}</p>
    <p> Cash Balance:{account_balance.cash_balance}</p>
   </Col>
 }
  </Col>
    </Row>

    </Well>

     </Grid>
    );
  }
}
TransactsPage.contextTypes = {
  router: PropTypes.object.isRequired
};
TransactsPage.propTypes = {
  actions: PropTypes.object.isRequired,
  account_balance: PropTypes.object.isRequired,
  alltransactions: PropTypes.array.isRequired,
  customerprofile: PropTypes.object.isRequired,
  userId: PropTypes.number.isRequired,
  //customeraccountbalance: PropTypes.object.isRequired,
};

function mapStateToProps(state, ownProps) {
  return {
    account_balance: state.assets.account_balance,
    alltransactions: state.assets.alltransactions,
    userId: state.authen.userId,
    customerprofile: state.authen.customerprofile,
    //customeraccountbalance: state.assets.customeraccountbalance
  };
}
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  };
}
export default connect(mapStateToProps,mapDispatchToProps)(TransactsPage);
