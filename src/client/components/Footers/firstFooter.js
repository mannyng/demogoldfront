import React from 'react';
import {Nav,NavItem,NavDropdown,MenuItem,Well,Col,Row,Grid} from 'react-bootstrap';

class FirstFooter extends React.Component {
  handleSelect(eventKey) {
    event.preventDefault();
    alert(`selected ${eventKey}`);
  }

  render() {
    return (
      <Grid>
      <Well bsSize="large">
      <Row className="show-grid">
      <Col md={4} xs={4}>
      <p>Wesern</p>
      </Col>
      <Col md={8} xs={8}>
      <Nav bsStyle="tabs" activeKey="1" onSelect={k => this.handleSelect(k)}>
        <NavItem eventKey="1" href="/home">
          Home
        </NavItem>
        <NavItem eventKey="2" href="/home">
          Trades
        </NavItem>
        <NavItem eventKey="3" href="/home">
          Networks
        </NavItem>
        <NavItem eventKey="4" href="/home">
          Contact Us
        </NavItem>
       </Nav>
       </Col>
      </Row>
      </Well>
      <Row className="show-grid">
         <Col md={4} xs={4}>
           copyright
         </Col>
         <Col md={4} xs={4}>
            <p>Dont miss my site: <a href="www.devbe.com">DevButze</a> </p>
         </Col>
         <Col md={4} xs={4}>
           Shining right
         </Col>
      </Row>
      </Grid>
    );
  }
}

export default FirstFooter;
