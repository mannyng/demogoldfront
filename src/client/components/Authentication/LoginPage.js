import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import * as actions from './authActions';
//import { asigninUser,resetAuth} from '../Home/Home.Actions';
import {Grid,Row,Col} from 'react-bootstrap';

class LoginPage extends React.Component {
    constructor(props) {
        super(props);

        // reset login status
        //this.props.actions.resetAuth();

        this.state = {
            email: '',
            password: '',
            submitted: false,
            loggingIn: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });
    }

    handleSubmit(e) {
        e.preventDefault();

        this.setState({ submitted: true });
        const { email, password } = this.state;
        const values = {email,password};
        const { dispatch } = this.props;
        if (email && password) {
          debugger;
            this.props.actions.signinUser(values,this.context.router.history);
        }
    }

    render() {
        //const { loggingIn } = this.props;
        const { email, password, submitted,loggingIn  } = this.state;
        return (
          <Grid>
          <Row className="show-grid">
          <Col xs={12} md={3}/>
          <Col xs={12} md={6}>

          </Col>
          </Row>
          <Row className="show-grid">
          <Col xs={12} md={6} smOffset={3}>

                <h2>Login</h2>
                <form name="form" onSubmit={this.handleSubmit}>

                    <Col className={'form-group' + (submitted && !email ? ' has-error' : '')}>
                    <Col xs={12} md={12} style={{marginBottom:10}}>
                    <Col md={3}>
                        <label htmlFor="email">Email</label>
                    </Col>
                        <Col md={8}>
                        <input type="text" className="form-control" name="email" value={email} onChange={this.handleChange} />
                        {submitted && !email &&
                            <div className="help-block">Email is required</div>
                        }
                        </Col>
                      </Col>
                    </Col>

                    <Col className={'form-group' + (submitted && !password ? ' has-error' : '')} >
                    <Col xs={12} md={12} style={{marginBottom:20}}>
                      <Col md={3}>
                          <label htmlFor="password">Password</label>
                      </Col>
                      <Col md={8}>
                         <input type="password" className="form-control" name="password" value={password} onChange={this.handleChange} />
                         {submitted && !password &&
                          <div className="help-block">Password is required</div>
                         }
                        </Col>
                      </Col>
                    </Col>
                    <Col className="form-group">
                        <button className="btn btn-primary">Login</button>
                        {loggingIn &&
                            <img src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==" />
                        }
                        <Link to="/register" className="btn btn-link">Register</Link>
                    </Col>
                </form>

            </Col>
            </Row>
            </Grid>
        );
    }
}
LoginPage.contextTypes = {
  router: PropTypes.object.isRequired
};
LoginPage.propTypes = {
  //loggingIn: PropTypes.bool.isRequired,
  actions: PropTypes.object.isRequired,
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  };
}

export default connect(null, mapDispatchToProps)(LoginPage);
//export default (LoginPage);
