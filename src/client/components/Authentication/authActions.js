import * as types from '../../constants/actionTypes';
import axios from 'axios';
import localStorage from 'localStorage';
import {beginAjaxCall, ajaxCallError} from '../../constants/ajaxStatusActions';
import toastr from 'toastr';
import jwt_decode from 'jwt-decode';

export function fetchUserSuccess(profile) {
   //debugger;
    return { type: types.FETCH_USER_SUCCESS, profile};
 }

 export function signinUser({ email, password},history) {
  return function(dispatch, getState) {
        debugger;
      dispatch(beginAjaxCall());
    // Submit email/password to the server
    return axios.post(`${types.ROOT_URL}/users/login`, { email, password })
      .then(response => {
          debugger;
        dispatch({ type: types.AUTH_USER });
        localStorage.setItem('token', response.data.auth_token);
        const userId = jwt_decode(response.data.auth_token).user_id;
        const emailID = jwt_decode(response.data.auth_token).email_id;
        dispatch (authenEmailSuccess(emailID))
        dispatch (authenUserIdSuccess(userId))
        //dispatch(authenTokenSuccess(response.data.auth_token));
        toastr.success('Logged In');
        //dispatch(fetchCustomerProfiles(userId,MAPI_HEADERS));
        history.push('/users');
      })
      .catch(error => {
        //dispatch(ajaxCallError(error));
        debugger;
        toastr.error('Bad Login Info, Make sure you are signing with correct Email and Password');
        throw(error);
      });
  };
}

export function registerUser({ email, password, password_confirmation, remember_me }, history) {
  return function(dispatch) {
    debugger;
    axios.post('http://localhost:3002/users', { email, password, password_confirmation, remember_me })
      .then(response => {
        toastr.success('User registered successfully');
        history.push('/');
      })
      .catch(error => {
        debugger;
        //dispatch(authError('Sign up Info'));
        toastr.error('Bad Sign up Info, Make sure you are using Email address');
        throw(error);
      });
  };
}

export function authenTokenSuccess(auth_token) {
    return {
      type: types.AUTHEN_TOKEN_SUCCESS,
      auth_token: auth_token
    };
  }
  export function authenEmailSuccess(emailID) {
      return {
        type: types.AUTHEN_EMAIL_SUCCESS,
        emailID: emailID
      };
    }
    export function authenUserIdSuccess(userId) {
        return {
          type: types.AUTHEN_USERID_SUCCESS,
          userId: userId
        };
      }
 //LOG OUT FUNCTION
export function resetAuth(history){
  return function(dispatch){
      dispatch({ type: types.RESET_AUTHEN_TOKEN});
      dispatch({type: types.UNAUTH_USER });
      localStorage.removeItem('token');
      //dispatch(changeAppRoot('login'));
      toastr.success('Signed out');
      history.push('/');
    };
}
//AUTHENTICATED REDIRECT TO HOME
export function authRedirect(history){
  return function(dispatch){
    toastr.success('REDIRECTED');
    history.push('/');
    window.location.href = window.location.href;
    };
}
function theAuthRedirect(history){
  return function(){
      toastr.success('REDIRECTED');
      history.push('/');
      window.location.href = window.location.href;
    };
}
export function fetchUserProfiles() {
    const mytoken = localStorage.getItem('token');
    const GAPI_HEADERS = {
         'Content-Type': 'application/json',
         'Authorization': `Bearer ${mytoken}`
     };
    //debugger;
    return function(dispatch) {
        dispatch(beginAjaxCall());
        //return axios.get(`${types.ROOT_URL}/customers/${user}`,
        return axios.get(`${types.ROOT_URL}/users`,
        //{headers: GAPI_HEADERS }).then(profile => {
        ).then(profile => {
            debugger;
            dispatch(fetchUserSuccess(profile.data));
        }).catch(error => {
           dispatch(ajaxCallError(error));
           toastr.error('Error fetching your profile');
            throw(error);
        });
    };
}



export function recgisterUser({ email, password, password_confirmation, remember_me }, history) {
  return function(dispatch) {
    //debugger;
    axios.post(`${types.ROOT_URL}/users`, { email, password, password_confirmation, remember_me })
      .then(response => {
        debugger;
        toastr.success('User registered successfully');
        dispatch(fetchAllCustomers());
        history.push('/adminpage');
      })
      .catch(error => {
        //dispatch(authError('Sign up Info'));
        toastr.error('Bad Sign up Info, Make sure you are using Email address');
        throw(error);
      });
  };
}
