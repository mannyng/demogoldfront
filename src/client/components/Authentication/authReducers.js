import * as types from '../../constants/actionTypes';
import initialState from '../../initialState';

export default function (state = initialState.authen, action) {
    switch (action.type) {
    case types.AUTH_USER:
    //debugger;
    return {
        ...state,
        authenticated: true
        };
    case types.UNAUTH_USER:
    return {
        ...state,
        authenticated: false
        };
    case types.AUTH_ERROR:
    return {
        ...state,
        auth_error: action.payload
        };
    case types.FETCH_USER_SUCCESS:
     //debugger;
         return {
         ...state,
         currentUser: action.profile,
         customerID: action.customerID
         };
       case types.AUTHEN_TOKEN_SUCCESS:
        //debugger;
           return {
             ...state,
             auth_token: action.auth_token
           };
        case types.AUTHEN_EMAIL_SUCCESS:
        //debugger;
          return {
            ...state,
            emailID: action.emailID
          }
          case types.AUTHEN_USERID_SUCCESS:
          //debugger;
            return {
              ...state,
              userId: action.userId
            }
        case types.RESET_AUTHEN_TOKEN:
        //debugger;
             return {
                ...state,
                auth_token: initialState.authen.auth_token,
                userId: initialState.authen.userId,
                emailID: initialState.authen.emailID
            };
      case types.CHANGE_ACCESS_FILTER:
               return {
               ...state,
               accessFilter: action.accessFilter
       };
       case types.CREATE_USER_SUCCESS:
                return {
                ...state,
                customer: action.customer
        };
        case types.FETCH_CUSTOMER_PROFILE:
        debugger;
          return {
            ...state,
            customerprofile: action.customerprofile
          };
          case types.CREATE_CUSTOMER_SUCCESS:
          debugger;
            return {
              ...state,
              newcustomerprofile: action.newcustomerprofile
            };

     default:
       return state;
   }
}
