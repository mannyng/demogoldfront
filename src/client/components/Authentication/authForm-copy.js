import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {Grid,Row, Form ,Button,FormControl,Col,FormGroup,Checkbox,ControlLabel,HelpBlock } from 'react-bootstrap';
//import {Container,Button} from 'reactstrap';

const AuthForm = ({handleChange,_loginUser,email,password}) => {
//debugger;
  return (
    <Grid>
    <Row className="show-grid">
    <Col xs={12} md={3}/>
    <Col xs={12} md={6}>

    </Col>
    </Row>
    <Row className="show-grid">
    <Col xs={12} md={3}/>
    <Col xs={12} md={6}>
    <form name="form" onSubmit={_loginUser}>
   <FormGroup controlId="formHorizontalEmail" >
   <Col componentClass={ControlLabel} sm={2}>
     Email
   </Col>
   <Col sm={10}>
     <FormControl
      value={email}
      onChange={handleChange('email')}
     type="email" placeholder="Email" />
   </Col>
   <FormControl.Feedback />
 </FormGroup>

 <FormGroup controlId="formHorizontalPassword">
   <Col componentClass={ControlLabel} sm={2}>
     Password
   </Col>
   <Col sm={10}>
     <FormControl type="password" placeholder="Password"
     value={password}
     onChange={handleChange("password")}/>
   </Col>
   <FormControl.Feedback />
 </FormGroup>

 <FormGroup>
   <Col smOffset={2} sm={10}>
     <Checkbox>Remember me</Checkbox>
   </Col>
 </FormGroup>

   <Col smOffset={2} sm={10}>
     <button className="btn btn-primary">Sign in</button>
   </Col>

</form>
</Col>
</Row>
</Grid>
     )

}
AuthForm.propTypes = {
  _loginUser: PropTypes.func.isRequired,
  handleChange: PropTypes.func.isRequired,
  email: PropTypes.string.isRequired,
  password: PropTypes.string.isRequired,
  //getValidationState: PropTypes.func.isRequired
};

export default AuthForm;
