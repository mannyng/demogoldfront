import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {Grid,Row, Form ,Button,FormControl,Col,FormGroup,Checkbox,ControlLabel,HelpBlock } from 'react-bootstrap';
//import {Container,Button} from 'reactstrap';

const RegisterForm = ({handleChange,_registerUser,email,password,password_confirmation,remember_me}) => {
//debugger;
  return (
    <Grid>
    <Row className="show-grid">
    <Col xs={12} md={3}/>
    <Col xs={12} md={6}>

    </Col>
    </Row>
    <Row className="show-grid">
    <Col xs={12} md={3}/>
    <Col xs={12} md={6}>
    <Form horizontal onSubmit={_registerUser}>
   <FormGroup controlId="formHorizontalEmail" >
   <Col componentClass={ControlLabel} sm={2}>
     Email
   </Col>
   <Col sm={10}>
     <FormControl
      value={email}
      onChange={handleChange('email')}
     type="email" placeholder="Email" />
   </Col>
   <FormControl.Feedback />
 </FormGroup>

 <FormGroup controlId="formHorizontalPassword">
   <Col componentClass={ControlLabel} sm={2}>
     Password
   </Col>
   <Col sm={10}>
     <FormControl type="password" placeholder="Password"
     value={password}
     onChange={handleChange("password")}/>
   </Col>
   <FormControl.Feedback />
 </FormGroup>
 <FormGroup controlId="formHorizontalPasswordConfirmation">
   <Col componentClass={ControlLabel} sm={2}>
     Password
   </Col>
   <Col sm={10}>
     <FormControl type="password" placeholder="Password Confirmation"
     value={password_confirmation}
     onChange={handleChange("password_confirmation")}/>
   </Col>
   <FormControl.Feedback />
 </FormGroup>

 <FormGroup>
   <Col smOffset={2} sm={10}>
     <Checkbox value={remember_me}
     onChange={handleChange("remember_me")}>Remember me</Checkbox>
   </Col>
 </FormGroup>

 <FormGroup>
   <Col smOffset={2} sm={10}>
     <Button type="submit">Sign in</Button>
   </Col>
 </FormGroup>
</Form>
</Col>
</Row>
</Grid>
     )

}
RegisterForm.propTypes = {
  _registerUser: PropTypes.func.isRequired,
  handleChange: PropTypes.func.isRequired,
  email: PropTypes.string.isRequired,
  password: PropTypes.string.isRequired,
  //getValidationState: PropTypes.func.isRequired
};

export default RegisterForm;
