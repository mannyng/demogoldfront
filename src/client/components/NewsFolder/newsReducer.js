import * as types from '../../constants/actionTypes';
import initialState from '../../initialState';

export default function (state = initialState.news, action) {
    switch (action.type) {
        case types.SCRAPE_MARKET_NEWS_SUCCESS:
                 return {
                 ...state,
                 marketnews: action.marketnews
         };
         case types.SCRAPE_BALTIC_NEWS_SUCCESS:
                  return {
                  ...state,
                  balticnews: action.balticnews
          };
          case types.FETCH_ALL_NEWS_SUCCESS:
                   return {
                   ...state,
                   allnews: action.allnews
           };

      default:
        return state;
    }
  }
