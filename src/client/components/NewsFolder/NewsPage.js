import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import {connect} from 'react-redux';
import * as actions from './newsAction';
import { Grid,Button,Well,Col,Row} from 'react-bootstrap';

class NewsPage extends React.Component{
  constructor(props) {
    super(props);

    this.state = {
      newsUrl: '',
      news: [],
      submitted: false,
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleSubmitBaltic = this.handleSubmitBaltic.bind(this);
  }
  componentWillMount() {
   this._checkNews();
  }

  _checkNews() {
    const {newsUrl} = this.state;
   this.props.actions.fetchAllNews();
  }
  handleChange(e) {
      const { name, value } = e.target;
      this.setState({ [name]: value });
  }

  handleSubmit(e) {
      e.preventDefault();

      this.setState({ submitted: true });
        debugger;
          this.props.actions.goScrapeMarketNews(this.context.router.history);
  }
  handleSubmitBaltic(e) {
      e.preventDefault();

      this.setState({ submitted: true });
      //const { newsUrl } = this.state;
      //const values = {newsUrl};
      //const { dispatch } = this.props;
      //if (newsUrl) {
        debugger;
          this.props.actions.goScrapeBalticNews(this.context.router.history);
      //}
  }

  render(){
    const {marketnews,balticnews,allnews} = this.props;
    const {newsUrl,submitted} = this.state;
   //debugger;
  return(
    <Grid style={{padding: 20}}>
    <Well bsSize="large">
    <Row className="show-grid">
    <Col md={6} xs={12}>
    <form name="form" onSubmit={this.handleSubmit}>
        <Col className="form-group">
            <button className="btn btn-primary">Scrape MarketWatch</button>
         </Col>
      </form>
    </Col>
    <Col md={6} xs={12}>
    <form name="form" onSubmit={this.handleSubmitBaltic}>
    <Col className="form-group">
        <button className="btn btn-primary">Scrape BalticNews</button>
     </Col>
     </form>
    </Col>
    </Row>
    <Row className="show-grid">
     <Col md={6} xs={12}>
      {allnews.map((news, index) =>
        <ul  key={index}>
        {news.href &&
          <Row className="show-grid">
        <p>{news.h2}</p>
        <h5><a href={news.href}>Read more</a></h5>
        </Row>
        }
        </ul>
      )}
      </Col>
      <Col md={6} xs={12}>
      {allnews.map((news, index) =>
        <div  key={index}>
        {news.fat_href &&
          <Row className="show-grid">
        <p>{news.fat_h2}</p>
         <h5><a href={news.news_url+news.href}>Read more</a></h5>
         </Row>
       }{news.slim_href &&
         <Row className="show-grid">
         <h5><a href={news.news_url+news.slim_href}>{news.slim_h5}</a></h5>
         </Row>
       }
        </div>
      )}
      </Col>
      </Row>
    </Well>

     </Grid>
    );
  }
}
NewsPage.contextTypes = {
  router: PropTypes.object.isRequired
};
NewsPage.propTypes = {
  marketnews: PropTypes.array.isRequired,
  balticnews: PropTypes.array.isRequired,
  allnews: PropTypes.array.isRequired,
};

function mapStateToProps(state, ownProps) {
  return {
    marketnews: state.news.marketnews,
    balticnews: state.news.balticnews,
    allnews: state.news.allnews
  };
}
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  };
}
export default connect(mapStateToProps,mapDispatchToProps)(NewsPage);
