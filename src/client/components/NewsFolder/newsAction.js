import * as types from '../../constants/actionTypes';
import axios from 'axios';
import toastr from 'toastr';

//SCRAPE ALL NEWS
export function fetchAllNews(){
  return function(dispatch){
    axios.get(`${types.ROOT_URL}/news`)
      .then(response => {
          debugger;
          dispatch(fetchAllNewsSuccess(response.data));
        })
        .catch(error => {
          debugger;
          throw(error);
        })
  };
}
export function fetchAllNewsSuccess(allnews){
  debugger;
  return {
    type: types.FETCH_ALL_NEWS_SUCCESS,
    allnews: allnews
  }
}
//SCRAPE MARKET NEWS
export function goScrapeMarketNews(newsUrl){
  return function(dispatch){
    axios.get(`${types.ROOT_URL}/news/check_news`)
      .then(response => {
          debugger;
          dispatch(scrapeMarketNewsSuccess(response.data.newsUrl));
        })
        .catch(error => {
          debugger;
          throw(error);
        })
  };
}

export function scrapeMarketNewsSuccess(marketnews){
  debugger;
  return {
    type: types.SCRAPE_MARKET_NEWS_SUCCESS,
    marketnews: marketnews
  }
}
//SCRAPE BALTIC NEWS
export function goScrapeBalticNews(newsUrl){
  return function(dispatch){
    axios.get(`${types.ROOT_URL}/news/check_baltic_news`)
      .then(response => {
          debugger;
          dispatch(scrapeBalticNewsSuccess(response.data.newsUrl));
        })
        .catch(error => {
          debugger;
          throw(error);
        })
  };
}

export function scrapeBalticNewsSuccess(balticnews){
  debugger;
  return {
    type: types.SCRAPE_BALTIC_NEWS_SUCCESS,
    balticnews: balticnews
  }
}
